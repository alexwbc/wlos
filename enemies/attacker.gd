extends Node2D

var main
var db
var game
var player
var enemy_matrix

var entity
var health
var y_range
var x_range

var old_pos
var old_anim

#animstuff
var idle_anim_list#get the list of animation aviable for idle (the will pick randomly)
var anim_delay = [0,1]

#IA
var status = 0#0 idle, walking the line

var path
var follow_path

var path_orig_pos
var path_dest_pos

#gameplay
var speed = 0.3


#development
var delay = [0,1]

func update_path():

#	var path_orig_pos = get_global_position()
#	path_dest_pos = Vector2(int(rand_range(x_range[0]*main.screen_x_ratio,x_range[1]*main.screen_x_ratio)),int(rand_range(y_range[0]*main.screen_y_ratio,y_range[1]*main.screen_y_ratio)))
#	path_dest_pos -= path_orig_pos
#	var curve = path.get_curve()
#	curve.clear_points()
#	var orig_mid = Vector2(lerp(0,path_dest_pos.x, 0.3),path_dest_pos.y)#y from target, x some
#	var target_mid = Vector2(lerp(0,path_dest_pos.x, 0.7),0)#y from origin, x some
#	curve.add_point(Vector2(0,0), -orig_mid, orig_mid)
#	curve.add_point(path_dest_pos, target_mid, -target_mid)

	follow_path.set_unit_offset(0)


	var path_orig_pos = get_global_position()
	var path_dest_pos = Vector2(int(rand_range(x_range[0]*main.screen_x_ratio,x_range[1]*main.screen_x_ratio)),int(rand_range(y_range[0]*main.screen_y_ratio,y_range[1]*main.screen_y_ratio)))

	var rel_difference = path_dest_pos-path_orig_pos#relative difference between the two

	var orig_mid = Vector2(rel_difference.x*0.3, rel_difference.y)#y from target relative
	var target_mid = Vector2(rel_difference.x*0.7, 0)#y 0
	var curve = path.get_curve()
	curve.clear_points()
	curve.add_point(path_orig_pos, -orig_mid, orig_mid)
	curve.add_point(path_dest_pos, target_mid, -target_mid)
	


func dev(delta):
	if delay[0] > delay[1]:
		delay[0] = 0
		print("updating")
		update_path()
	delay[0] += 1*delta


func dev_bk(delta):
	if delay[0] > delay[1]:
		delay[0] = 0
		update_path()

	follow_path.set_unit_offset(randf())
	get_node("origin").set_position(follow_path.get_position())

	delay[0] += 1*delta

func _enter_tree():
	idle_anim_list = get_node("anim/idle").get_animation_list()
	old_pos = get_global_position()
	set_path()
	set_process(true)
	set_meta("attacker", 0)
	game.get_node("enemies").add_child(get_node("."))

	get_node("ship").set_meta("ship", 0)#		fireline segment use these to
	get_node("shield").set_meta("shield", 0)#	identify if target is ship or shield
	
	
func set_path():
	path = Path2D.new()
	path.get_curve().add_point(get_global_position())
	add_child(path)
	path.set_owner(get_node("."))
	follow_path = PathFollow2D.new()
	path.add_child(follow_path)
	follow_path.set_owner(path)
	update_path()


func walking_line(delta):

	var offset = follow_path.get_unit_offset()
	if offset >= 0.98:
		follow_path.set_unit_offset(0)
		update_path()
		print("done.return")
		return

	offset += speed*delta
	follow_path.set_unit_offset(offset)

	set_position(follow_path.get_position())


func _process(delta):
	walking_line(delta)

	var pos = get_global_position()
	var playerpos = player.get_global_position()
	var anim = "goingdn"
	if pos.y > old_pos.y:
		anim = "goingup"

	if (anim != old_anim) and (anim_delay[0] <= 0):
		get_node("anim").play(anim)
		anim_delay[0] = anim_delay[1]
	else:
		anim_delay[0] -= 1*delta
	old_pos = pos
	old_anim = anim


func fix_direction(area):
#	update_path()
#	var inverted_direction = old_pos+(old_pos-area.get_global_position())
	
	follow_path.set_unit_offset(0)
	

	var path_orig_pos = get_global_position()
	var path_dest_pos = old_pos+(old_pos-area.get_global_position())
	if path_dest_pos.x > x_range[1]*main.screen_x_ratio:
		path_dest_pos.x = x_range[1]*main.screen_x_ratio
	var rel_difference = path_dest_pos-path_orig_pos#relative difference between the two

	var orig_mid = Vector2(rel_difference.x*0.3, rel_difference.y)#y from target relative
	var target_mid = Vector2(rel_difference.x*0.7, 0)#y 0
	var curve = path.get_curve()
	curve.clear_points()
	curve.add_point(path_orig_pos, -orig_mid, orig_mid)
	curve.add_point(path_dest_pos, target_mid, -target_mid)





#	var it = get_node("orig_mid")
#	var me = get_node("target_mid")
#	it.set_global_position(area.get_global_position())
#	me.set_global_position(inverted_direction)

#	var message = str(area.get_global_position()) + "\n" + str(get_global_position())
#	get_node("Label").set_text(str(inverted_direction))


func _on_attacker_area_shape_entered( area_id, area, area_shape, self_shape ):
	if area.has_meta("attacker"):
		fix_direction(area)
	elif area.has_meta("bullet"):
		got_hit(area.get_global_position())
#		area.fix_direction(get_node("."))

func got_hit(where):
	print(where)
	end()

func end():
	queue_free()




func _on_idle_animation_finished( name ):
	var anim = idle_anim_list[randi()%idle_anim_list.size()]
	get_node("anim/idle").play(anim)

