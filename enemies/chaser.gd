

###IMPORTANT: Kill list
##these nodes are parented outside, so queue_free is not enough to destroy them
#follow_path ## path


extends Area2D

#the "chaser" is the first manifestation of the enemy.
#it runs on background, when reach the end of the screen (right) it cast an enemy in front of the player



var main
var db
var game
var player
var enemy_matrix
var entity

var path
var follow_path
var old_pos = Vector2(0,0)
var old_curve = null


#motion
var speed = 0.3
var scale_min = Vector2(0.3, 0.5)#starting size (distant = tiny) will set randomly between two float
var scale_max = Vector2(0.6, 0.8)#ending size (closer = bigger) will set randomly between two float
var scalepath = Vector2(0,0)#resulting start and ending size
var min_speed = 0.2
var max_speed = 0.31
var ship_motion = Vector2(0,0)
#var crash_landing_motion = Vector2(0,0)

#gameplay stats
var health = 1



#animation stuff
const idle_limit = 2
var old_anim = "goup"

var underfire_target#the node that's targeting


#machine states
var health_process = 0#3 = death
var under_fire = false
var firerate_delay = 0


#health stuff
var healthbar
var healthbar_basescale
var health_delay = [0,0.4]


#fx
var single_burst
var flamesmoke
var explosion#OnWork
#var debris

#path follow generation
var y_range = Vector2(0,0)#¦enemy_matrix will set these for us
var x_steps = [30,60, 130]#¦

#path
var origin#the point (out of screen) where it start
var target#the point (out of screen) where it run away


var collision

var x_step_process = 0

var lazy_process = [0,0.01]

func _enter_tree():
	set_process(true)
#	single_burst = preload("res://fx/singleburst.tscn")
	single_burst = preload("res://draft/fxgeneration/burst/burst.tscn")
#	debris = preload("res://draft/fxgeneration/shipdestroy/debris.tscn")
	flamesmoke = preload("res://fx/particles/flamesmoke.tscn")
	explosion = preload("res://draft/fxgeneration/explode/explosion.tscn")
	origin = get_global_position()
	healthbar = get_node("healthbar")
	healthbar.set_modulate(Color(1,1,1,0))
	healthbar_basescale = healthbar.get_scale()
	set_path()
	scalepath = Vector2(rand_range(scale_min.x, scale_min.y), rand_range(scale_max.x, scale_max.y))
	


func set_path():
	path = Path2D.new()
	game.add_child(path)
	path.set_owner(game)
	path.get_curve().add_point(origin)
	var curves = []
	for i in range(x_steps.size()):
		curves.resize(1+i)
		curves[i] = Vector2(x_steps[i]*main.screen_x_ratio, int(rand_range(y_range[0]*main.screen_y_ratio,y_range[1]*main.screen_y_ratio)))
		
		if i > 0:
			curves[i].y -= (curves[i].y-curves[i-1].y)*0.6

	for i in range(curves.size()):
		var incurve = Vector2(0,0)
		var outcurve = Vector2(0,0)
		if i > 0:#incurve
			incurve.x = (curves[i-1].x-curves[i].x)*0.5
		if i < 2:#outcurve
			outcurve.x = (curves[i+1].x-curves[i].x)*0.5
		path.get_curve().add_point(curves[i], incurve, outcurve)

	follow_path = PathFollow2D.new()
	path.add_child(follow_path)
	follow_path.set_owner(path)




func walking_the_line(delta):
	var offset = follow_path.get_unit_offset()
	if offset <= 0.9:
		var invspd = (1-game.ship_percent_x)*(max_speed-min_speed)+min_speed
		
		offset += invspd*delta
		var offsetscale = scalepath[0]+(scalepath[1]*offset)


		var hb_scale = (1-(scalepath[1]*offset))*healthbar_basescale[0]
		healthbar.set_scale(Vector2(hb_scale,hb_scale))


		set_scale(Vector2(offsetscale, offsetscale))
		
	else:
		end()

	follow_path.set_unit_offset(offset)
	var pos = follow_path.get_position()
	set_position(follow_path.get_position())
	
	var anim = "idle"

	ship_motion = pos - old_pos
	if ship_motion.y > idle_limit:#is going up
#		print("is going up with: " + str(ship_motion.y))
		anim = "godn"
	elif ship_motion.y < -idle_limit:#is going down
		anim = "goup"

#	get_node("Label").set_text(str(health))



#	var diff = old_pos.y - pos.y
#	if diff > idle_limit:#dw
#		anim = "goup"
#	elif diff < -idle_limit:
#		anim = "godn"
		


	old_pos = pos
	if anim != old_anim:
		get_node("AnimationPlayer").play(anim)
	old_anim = anim

func end():
	if enemy_matrix != null:
		enemy_matrix.fire_fn_entity(get_global_position(), entity, health)
	follow_path.queue_free()
	path.queue_free()
	queue_free()


func process_death(delta):
	if health_process == 3:#
		ship_motion.y = max(-2, ship_motion.y)
#		add_child(debris.instance())
		add_child(flamesmoke.instance())
		var exp_node = explosion.instance()
		exp_node.set_global_position(main.cursor_pos)
		exp_node.set_z(get_z())
		game.add_child(exp_node)
		healthbar.set_value(0)
		health_process = 4
	elif health_process == 4:
		set_global_position(ship_motion+get_global_position())
		ship_motion.y+= 6*delta
		ship_motion.x-= 6*delta
#		pos += crash_landing_motion
#		set_global_position(get_global_position()+crash_landing_motion)
func _process(delta):

	if health_process >= 3:

		process_death(delta)
		return
	if firerate_delay < player.fp_secondary[1]:
		firerate_delay += 1*delta
	elif under_fire:
		got_hit(player.fp_secondary[0])
		firerate_delay = 0
		health_delay[0] = health_delay[1]

#health will not regenerate but hide if not under fire after short time
	if health_delay[0] > 0:
		health_delay[0] -= 1*delta
	else:
		healthprocess(delta)



	walking_the_line(delta)
	if health_process != 0:
		healthprocess(delta)


func healthprocess(delta):
	var opacity = healthbar.get_modulate()
	if opacity.a > 0:
		opacity.a -= 1*delta
		healthbar.set_modulate(opacity)
	

func got_hit(ammount):
	if underfire_target == null:
		return
	if !underfire_target.is_monitorable():
		under_fire = false
		return
	var hitspot = single_burst.instance()
	hitspot.set_global_position(underfire_target.get_global_position())
	hitspot.set_z(get_z())
	game.add_child(hitspot)
	if (health-ammount) <= 0:#die
		health_process = 3
		health = 0
		return
	health -= ammount
	healthbar.set("value", health)
	healthbar.set_modulate(Color(1,1,1,1))
	



func _on_chaser_area_shape_entered( area_id, area, area_shape, self_shape ):
	if area == null:
		return
	if health_process >= 3:
		return
	if area.has_meta("secondary_fire"):
		under_fire = true
		underfire_target = area


func _on_chaser_area_shape_exited( area_id, area, area_shape, self_shape ):
	if area == null:#sometime stuff exit becouse doesn't exist anymore. fix
		return

	if area.has_meta("secondary_fire"):
		under_fire = false
