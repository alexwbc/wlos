extends Node

var db
var scene_script = [
     {"id":00}
     ,{"id"               : 44
     ,"step"             : 1
     ,"portait_hero"     : "hero_happy"
     ,"portait_guest"    : "anna_angry"
     ,"text"             : "This is an event speech"
     ,"hero_speech"      : true
     }
     ,{
     "id"                : 44
     ,"step"             : 2
     ,"portait_hero"     : "hero_annoy"
     ,"text"             : "...now testing NPC reply"
     ,"hero_speech"      : false
     ,"hero_pos"         : Vector2(30,40)
     }
     ,{
     "id"                : 44
     ,"step"             : 4
     ,"portait_guest"    : "anna_frown"
     ,"text"             : "ah yeah, we got something to do about it"
     ,"hero_speech"      : false
     }
     ,{
     "id"                : 44
     ,"step"             : 5
     ,"portait_hero"     : "hero_happy"
     ,"text"             : "Uh? Wha--? No, of course: I was just expecting some sort of confirmation on--"
     ,"hero_speech"      : true
     }
     ,{
     "id"                : 44
     ,"step"             : 6
     ,"portait_guest"    : "anna_happy"
     ,"text"             : "Confirmation, you surely are a big boy to look for confirmation, aren't you?"
     ,"hero_speech"      : false
     }
     ,{
     "id"                : 2
     ,"ship_loc"         : Vector2(10,70)
     }
########################################
     ,{
     "id"                : 33
     ,"step"             : 1
     ,"portait_hero"     : "hero_happy"
     ,"portait_guest"    : "anna_angry"
     ,"text"             : "Hello NPC"
     ,"hero_speech"      : true
     }

     ,{
     "id"                : 33
     ,"step"             : 2
     ,"text"             : "Hello to you, random placehold for the hero"
     ,"hero_speech"      : false
     }
     ,{
     "id"                : 33
     ,"step"             : 3
     ,"portait_hero"     :"hero_annoy"
     ,"text"             : "random... what are you talking about? I am 100% granted hero here"
     ,"hero_speech"      : true
     }
     ,{
     "id"                : 33
     ,"step"             : 4
     ,"portait_guest"     :"anna_frown"
     ,"text"             : "I do surely doubt, look.. you're not even finished yet; this scream for dummy placehold to me"
     ,"hero_speech"      : false
     }
     ,{
     "id"                : 33
     ,"step"             : 5
     ,"portait_hero"     : "hero_happy"
     ,"text"             : "Never judge by apparence. I am an hero, and surely heroes don't wear shiny armor nowdays. The humblest, the mightiest"
     ,"hero_speech"      : true
     }
     ,{
     "id"                : 33
     ,"step"             : 6
     ,"portait_guest"    : "anna_happy"
     ,"text"             : "That's like... anyone can be hero nowday. Also... calling yourself as the mightiest don't suits you THAT humble"
     ,"hero_speech"      : false
     }
     ,{
     "id"                : 33
     ,"step"             : 7
     ,"text"             : "You can't set me down. See, I am still standing here set as \"happy\""
     ,"hero_speech"      : true
     }
     ,{
     "id"                : 33
     ,"step"             : 8
     ,"portait_hero"     : "hero_annoy"
     ,"text"             : "that's becouse I am humble, you know: admire my adamantine humbleness"
     ,"hero_speech"      : true
     }
     ,{
     "id"                : 33
     ,"step"             : 9
     ,"portait_guest"    :"anna_frown"
     ,"text"             : "awww... forget it. You're probably that dumb to get it anyway"
     ,"hero_speech"      : false
     }



########################################
]


func _enter_tree():
	db.scene_script = scene_script
	queue_free()

