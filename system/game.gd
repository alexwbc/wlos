extends Node2D

var db
var main


var death_request = false

#node specific
var background = null

#modules
var novel = null
var enemy_matrix = null

#intrgration with system
var sub_process = 0


#relative to ship
#var ship_percent = null
#var ship_percent_x = null
#var ship_percent_y = null
var ship_percent = 0
var ship_percent_x = 0
var ship_percent_y = 0

func _enter_tree():
	main = get_node("/root/main")
	db = main.database
	set_process(false)
	

#add a module
	#camera fx
	var camera_fx = Node.new()
	var script = load("res://system/modules/camera_mod.gd")
	camera_fx.set_script(script)
	camera_fx.camera = get_node("camera")
	camera_fx.game = get_node(".")
	camera_fx.main = main
	get_node("module").add_child(camera_fx)

#module novel
#	novel = Node.new()
#	novel.set_script(load("res://system/modules/novel.gd"))
	novel = load("res://system/modules/novel.tscn").instance()
	novel.main = main
	novel.db = db
	novel.game = get_node(".")
	get_node("module").add_child(novel)
	
#module enemy matrix
	enemy_matrix = load("res://system/modules/enemy_matrix.tscn").instance()
	enemy_matrix.main = main
	enemy_matrix.db = db
	enemy_matrix.game = get_node(".")
	get_node("module").add_child(enemy_matrix)


func resize():
#	print("lets' do the resize")
	if main.player != null:
		main.player.set_limit_x = Vector2(main.player.limit_x[0]*main.screen_x_ratio, main.player.limit_x[1]*main.screen_x_ratio)
		main.player.set_limit_y = Vector2(main.player.limit_y[0]*main.screen_y_ratio, main.player.limit_y[1]*main.screen_y_ratio)
		main.player.field_size = Vector2(main.player.set_limit_x[1]-main.player.set_limit_x[0], main.player.set_limit_y[1]-main.player.set_limit_y[0])
#		print(main.player.set_limit_x)
#		print(main.player.set_limit_y)
		if main.player.gunner != null:
			var gun_prox = main.player.gunner.max_gun_proximity[1]
			main.player.gunner.max_gun_proximity[0] = main.player.gunner.max_gun_proximity[1]*main.screen_x_ratio


	

func _process(delta):
	if death_request:
		end()
#	print("db is: " +str(db) + "\n main is: " + str(main))





func end():#cleaning and killing
	if sub_process == 0:
		set_name("gointodie")
		main.game = null
		main.player.queue_free()
		main.player = null
		main.camera.clear_current()
		main.camera.queue_free()
		main.camera = null
		if main.active_mode == get_node("."):
			main.active_mode = null
		sub_process = 1
		queue_free()
		return
	elif sub_process == 1:
		sub_process = 0




func freeze():#paused
	pass

func broken_start():
	print("start is broken")

func run_battlefield():
#db.current_area (1)
#db.area {id:current_area}

	for i in range(db.area.size()):
		var area_ongoing = db.area[i]
		if area_ongoing.has("id"):
			if area_ongoing["id"] == db.current_area:

#				Background
				var dir = Directory.new()
				if !dir.file_exists(area_ongoing["background"]):
					broken_start()
					return
				var battlefield = load(area_ongoing["background"]).instance()
				battlefield.game = get_node(".")
				background = battlefield
				add_child(battlefield)

#				Ship
				if area_ongoing["ship"] == "normal":
					var ship = load("res://ship/shipone.tscn").instance()
					if main.player != null:
						main.player.die()
					main.player = ship
					ship.game = get_node(".")
					add_child(ship)

				else:
					pass
				if area_ongoing.has("entrance"):
					
					var pos = area_ongoing["entrance"]
					main.player.set_global_position(Vector2(pos.x*main.screen_x_ratio, pos.y*main.screen_y_ratio))

	if main.camera != null:
		main.camera.queue_free()
	main.camera = get_node("camera")
	main.camera.make_current()
	resize()

