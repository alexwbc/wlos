extends Node


var game
var main
var camera

var random = 1
var power = 5

func _enter_tree():
	
	set_process(true)


func _process(delta):
	if main.player != null:
		var shake = int(power*game.ship_percent_x)+1
		var camera_pos = Vector2(0,0)
		camera_pos.y += random
		random = randi()%shake
		camera_pos.x += random-1
		camera.set_position(camera_pos)
