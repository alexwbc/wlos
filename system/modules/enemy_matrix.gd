extends Node


#base nodes
var main#parent will set this for us
var db#parent will set this for us
var game#parent will set this for us


#background generator (the "entrance" of enemies and stuff on the background
var out_fov_percent = 10#
var height_range = Vector2(10,90)
var timeout = 4#time to wait before spawn a new enemy
var timeout_count = 0



var chaser
var sprites
var chaser_sprites

var front_enemy

func _enter_tree():
	set_process(true)
	preload_png()
	preload_chaser()
	preload_front_enemy()


func preload_front_enemy():
	front_enemy = preload("res://enemies/attacker.tscn")

func preload_chaser():
	chaser = preload("res://enemies/chaser.tscn")
	chaser_sprites = SpriteFrames.new()
	sprites = [preload("res://enemies/ships/01/01.webp"),preload("res://enemies/ships/01/02.webp"),preload("res://enemies/ships/01/03.webp")]
	for i in range(sprites.size()):
		chaser_sprites.add_frame("default", sprites[i], i)



func preload_webp():
	sprites = [preload("res://enemies/ships/01/01.webp"),preload("res://enemies/ships/01/02.webp"),preload("res://enemies/ships/01/03.webp")]

func preload_png():
	sprites = [preload("res://enemies/ships/01/01.png"),preload("res://enemies/ships/01/02.png"),preload("res://enemies/ships/01/03.png")]
	



func _process(delta):
	timeout_count += delta
	if timeout_count >= timeout:
		var behind_camera_x = -out_fov_percent*main.screen_x_ratio
		var behind_camera_y = int(rand_range(height_range[0]*main.screen_y_ratio,height_range[1]*main.screen_y_ratio))
		fire_bg_entity(Vector2(behind_camera_x, behind_camera_y), null, null)
		timeout_count = 0

func fire_bg_entity(cord, entity, health):
	var enemy = chaser.instance()
	enemy.main = main
	enemy.db = db
	enemy.game = game
	enemy.entity = entity
	enemy.y_range = height_range
	enemy.health = 100
	if health != null:
		enemy.health = health
	enemy.player = main.player
	enemy.enemy_matrix = get_node(".")
	enemy.set_global_position(cord)
	game.add_child(enemy)

func fire_fn_entity(cord, entity, health):
	var enemy = front_enemy.instance()
	enemy.main = main
	enemy.db = db
	enemy.game = game
	enemy.entity = entity
	enemy.y_range = height_range
	enemy.x_range = Vector2(60,90)
	
	enemy.health = 100
	if health != null:
		enemy.health = health
	enemy.player = main.player
	enemy.enemy_matrix = get_node(".")

	enemy.set_global_position(cord)
	game.add_child(enemy)
