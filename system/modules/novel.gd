extends Control

var main
var db
var game

var portait_hero
var portait_guest
var text_hero
var text_guest

var sub_process = 0
var portaits_dir = null

var active_novel = false
var script = []
var script_line = 0#called "step" in database

var press_any_key = false
var was_any_key = false
var lazy = 0


#typewriter
var typewriting = false
var tw_paper = null#where the typewriter will print (text_guest or text_hero)
var tw_speed = 100#speed of the typweriter
var tw_accel = [100,200]
var tw_cursor = 0

func _input(event):
	if event.is_class("InputEventMouseMotion"):
		
		return
	if !event.pressed:#a key is released
		was_any_key = true
		tw_speed = tw_accel[0]#typeweriter speed normal
		return

#anything from here is for key pressed (but mouse motion and key/button released)
	tw_speed = tw_accel[1]
	if was_any_key:
		press_any_key = true
		was_any_key = false

func _enter_tree():
	portait_hero = get_node("portait_hero")
	portait_guest = get_node("portait_guest")
	text_hero = get_node("text_hero")
	text_guest = get_node("text_guest")
	portaits_dir = Directory.new()
	portaits_dir.change_dir("res://portaits")

func _ready():
	set_process_input(false)
	set_process(false)

func typewriter(delta):
	var line_size = tw_paper.get_total_character_count()
	var line_cursor = tw_paper.get_visible_characters()
	if line_cursor >= line_size:#typwriter has finished
		sub_process = 2
	tw_cursor += tw_speed*delta
	tw_paper.set_visible_characters(int(tw_cursor))


func novel_process(delta):
	if sub_process == 0:
		if (script_line+1) > script.size():
			end()
			return
		var run_line = db.scene_script[script[script_line]]
		if run_line.has("hero_pos"):
			main.player.cinematic_mode(true, run_line["hero_pos"])
		if run_line["hero_speech"]:#the hero is speaking
			tw_paper = text_hero
			portait_guest.set_modulate(Color(1,1,1,0.5))
			portait_hero.set_modulate(Color(1,1,1,1))

		else:
			tw_paper = text_guest
			portait_hero.set_modulate(Color(1,1,1,0.5))
			portait_guest.set_modulate(Color(1,1,1,1))
		tw_paper.set_text(run_line["text"])
		tw_paper.set_visible_characters(0)
		if run_line.has("portait_hero"):
			if portaits_dir.file_exists(str(run_line["portait_hero"])+ ".png"):
				var portait = load("res://portaits/" + str(run_line["portait_hero"])+ ".png")
				portait_hero.set_texture(portait)
		if run_line.has("portait_guest"):
			if portaits_dir.file_exists(str(run_line["portait_guest"])+ ".png"):
				var portait = load("res://portaits/" + str(run_line["portait_guest"])+ ".png")
				portait_guest.set_texture(portait)

		tw_cursor = 0
		typewriting = true
		sub_process = 1
	elif sub_process == 1:
		typewriter(delta)
	elif sub_process == 2 and press_any_key:#wait for a key
		script_line += 1
		sub_process = 0

	#run typewriter
	#wait for typewriter
	#if more script lines, switch to the next. otherwise end novel
	pass

func _process(delta):
	main.get_node("label_debug").set_text(str(was_any_key))
	if active_novel:
		novel_process(delta)

	lazy += 1*delta
	if lazy > 1:
#		print("iam alive")
		lazy = 0
	
#	if !was_any_key and press_any_key:
#		print("key pressed once")
#		press_any_key = false
#	was_any_key = press_any_key
	press_any_key = false

func end():
#	print("kill novel")
#	return
	set_process(false)
	active_novel = false
	main.player.cinematic_mode(false, Vector2(0,0))
	hide()

func run(novelid):
	show()
	#enter anim
	var running_novel = null
	if str(novelid) == "debug":
		novelid = 1

	for i in range(db.novels.size()):
		if db.novels[i]["id"] == novelid:
			running_novel = db.novels[i]
	if running_novel == null:
		print("novel doesn't exist/not found: abort!")
		return
	script.resize(0)
#	print("script is : "  + str(script[0]))
	for line in range(db.scene_script.size()):
		var check_line = db.scene_script[line]

		if check_line["id"] == novelid:
#			print("add one")
			script.resize(script.size()+1)
			script[script.size()-1] = line
#	print("script size is : " + str(script.size()))
	if script.size() <= 0:
#		print("the scene request have no activity")
		return
	if !running_novel.has("ship_loc"):#fallof in case there's no ship location
		running_novel["ship_loc"] = Vector2(10,50)
	main.player.cinematic_mode(true, running_novel["ship_loc"])
	active_novel = true
	script_line = 0
	set_process(true)
	set_process_input(true)
	#cleanup
	portait_guest.set_texture(null)
	portait_hero.set_texture(null)
	text_guest.set_text("")
	text_guest.set_visible_characters(0)
	text_hero.set_text("")
	text_hero.set_visible_characters(0)
	tw_cursor = 0
	sub_process = 0
