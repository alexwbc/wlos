extends ParallaxBackground

#nodes
var db
var game

#offest settings
var scroll = Vector2(0,0)
const offset_min = 0
const offset_max = -300
var offset_speed = 1000
var offset_speed_mult = 2

func _enter_tree():
#	db = get_node("/root/main").database
	set_process(true)

func _ready():
	if game == null:
		queue_free()

func _process(delta):
	scroll(delta)

func scroll(delta):
	var multiplier = 1
	if game.ship_percent_x != null:
		multiplier = game.ship_percent_x*offset_speed_mult+1
	scroll.x += (-offset_speed*delta)*multiplier
	if game.ship_percent_y != null:
		scroll.y = offset_max*game.ship_percent_y
		
	set_scroll_offset(scroll)
	
