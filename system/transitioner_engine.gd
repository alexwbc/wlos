extends Node

var database
var main
var operating = null
var sub_process = 0#ready to accept new request
var request_queue = null

func _enter_tree():
	main = get_node("/root/main")
	set_process(false)

func _process(delta):
	if operating == null:
		sub_process = 0
		set_process(false)
		return
	elif operating == "main_menu":
		process_main_menu(delta)
	elif operating == "game":
		process_game(delta)

func process_game(delta):
	if sub_process == 1:#sent cleanup
		if main.active_mode != null:
			main.active_mode.end()
			
		sub_process = 2
	if sub_process == 2:#wait for finish
		if main.game == null:
#			main.game = main.get_node("game")
			sub_process = 3
		else:
			print("main game is not null yet")
	if sub_process == 3:#cleanup recieved
		var game = load("res://system/game.tscn").instance()
		main.active_mode = game
		main.game = game
		game.set_name("game")
		main.add_child(game)
		sub_process = 100
		return
	if sub_process >= 100:
		main.game.run_battlefield()
		operating = null
		return

func process_main_menu(delta):
	if sub_process == 1:#sent cleanup
		if main.active_mode != null:
			main.active_mode.end()
		var start_menu = load("res://system/startmenu.tscn").instance()
		main.active_mode = start_menu
		if main.has_node("main_menu"):
			main.get_node("main_menu").queue_free()
		start_menu.set_name("main_menu")
		main.add_child(start_menu)
		main.main_menu = start_menu
		sub_process = 100
		return
	if sub_process >= 100:
		operating = null
		return




func request_game():
	print("main game!")
	operating = "game"
	sub_process = 1
	set_process(true)

func request_mainmenu():
	
	print("main menu on going!")
	operating = "main_menu"
	sub_process = 1
	set_process(true)
	#send the kill to everything
	#perform the fx fade effect
	#load the menu node
	#place the menu node in main
	#freeze