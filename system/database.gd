extends Node

#system nodes
var main
var root
var database
var game
var ui

#related to battlefield wandering
var current_ship
var map


#related to the story

#saves
var current_area


#database
var area
var novels
var scene_script

func update_language():
	
	#scene dialogues
	
	pass

func system_load():#variables for setting as language will be loaded here
	pass


func initiate_areas():
	var areas = Node.new()
	areas.set_script(load("res://system/database/areas.gd"))
	areas.db = database
	add_child(areas)

func initiate_scene_scripts():
	var scene_script_node = Node.new()
	scene_script_node.set_script(load("res://system/database/scenes.gd"))
	scene_script_node.db = database
	add_child(scene_script_node)

func initiate_novels():
	var novel = Node.new()
	novel.set_script(load("res://system/database/novels.gd"))
	novel.db = database
	add_child(novel)

func _enter_tree():
	root = get_node("/root")
	database = get_node(".")
	set_process(true)

	initiate_areas()
	initiate_novels()
	initiate_scene_scripts()
#	ui
	debug()
	
#	print(scene_script)




func _process(delta):
	pass

func debug():#populate the database with stuff for testing
	current_area = 1
