extends Node



##main nodes
var root
var database
var transit

#nodes for menu
var main_menu

#nodes in game
var game = null
var camera = null
var player = null


var active_mode = null

var cursor
var cursor_pos
var viewport

#services
var mouse_relative = Vector2(0,0)
var mouse_pos = Vector2(0,0)
var hide_cursor = false

#screen values
var screen_x_margin
var screen_y_margin
var screen_x_ratio
var screen_y_ratio



func resize():
	print("let's do the resize")
	var new_resize = get_viewport().get_size()
	screen_x_margin = new_resize.x
	screen_y_margin = new_resize.y
	screen_x_ratio = screen_x_margin/100
	screen_y_ratio = screen_y_margin/100
	#game and main menu node
	if game != null:
		game.resize()
	if main_menu != null:
		main_menu.resize()



func _enter_tree():
	#initiate database
	database = load("res://system/database.tscn").instance()
	database.main = get_node(".")
	add_child(database)
	
	#main nodes
	root = get_node("/root")
#	game = get_node("game")
	cursor = get_node("cursor")
	viewport = get_viewport()
	transit = get_node("transitioner_engine")
	get_viewport().connect("size_changed", self, "resize")
	set_process(true)
	set_process_input(true)

	#player = get_node("player")
	cursor = get_node("cursor")
	
#	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	var new_resize = get_viewport().get_size()
	screen_x_margin = new_resize.x
	screen_y_margin = new_resize.y
	screen_x_ratio = screen_x_margin/100
	screen_y_ratio = screen_y_margin/100


func _ready():
#	transit.request_game()
#	game.run_battlefield()
	request("game")
	pass

func _process(delta):

	var cursor_opacity = cursor.get_self_modulate()
	if hide_cursor and (cursor_opacity.a > 0):
		cursor_opacity.a -= 10*delta
		cursor.set_self_modulate(cursor_opacity)
	elif !hide_cursor and (cursor_opacity.a < 1):
		cursor_opacity.a += 10*delta
		cursor.set_self_modulate(cursor_opacity)

	if Input.is_action_just_pressed("debug_1"):
		if player != null:
			player.inflict_damage(10)
	if Input.is_action_just_pressed("debug_2") and (active_mode != null):
		if active_mode.get_name() == "game":
			if game != null:
				if game.novel != null:
					if game.novel.active_novel:
						print("kill novel")
						game.novel.end()
					else:
						print("activate novel")
						game.novel.run(33)
			print(active_mode.get_pause_mode())
			active_mode.set_process(false)
	if Input.is_action_just_pressed("debug_3"):
		if game != null:
			var layer = game.background.get_layer()+1
			print(layer)
			game.background.set_layer(game.background.get_layer()-1)
			

	get_node("Button").raise()
#	var text = "cursor pos: " + str(cursor.get_global_position())
#	camera.get_camera_screen_center()
#	get_node("Label").set_text(str(cursor.get_global_position()))
	var text = "\nmouse: " + str(get_viewport().get_mouse_position())
	text += "\n--: " + str(cursor.get_global_position())
	text += "\nResolution: " + str(screen_x_margin) +"x"+str(screen_y_margin)
	text += "\nratio: " + str(screen_x_ratio) +"x"+str(screen_y_ratio)
	text += "\nxPercent: " + str(int(cursor.get_global_position().x/screen_x_ratio))
	if player != null:
		text += "\nship pos: " + str(int(player.get_global_position().x))+ "x"+str(int(player.get_global_position().y))
	if game != null:
		text += "\n--: " + str(game.ship_percent)
	if camera != null:
		text += "\n--: " + str(camera.get_camera_screen_center())
#	if hide_cursor
#	text += "\n--: " + str(camera.get_zoom())
#	text += "\n--: " + str(camera.get_zoom())
#	text += "\n--: " + str(camera.get_zoom())
#	text += "\n--: " + str(camera.get_zoom())



	get_node("Label").set_text(text)


func _input(ev):
	if ev.is_class("InputEventMouseMotion"):# == InputEveInputEvent.MOUSE_MOTION:
		if !hide_cursor:
			cursor_pos = cursor.get_position()
			cursor_pos += ev.relative
			cursor_pos = Vector2(max(min(screen_x_margin, cursor_pos.x), 0), max(min(screen_y_margin, cursor_pos.y), 0))
			cursor.set_global_position(cursor_pos)



func _on_Button_pressed():
	request("main_menu")


func request(what):
	if what == "main_menu":
		transit.request_mainmenu()
	if what == "game":
		transit.request_game()

func _on_menu2_pressed():
		request("game")
