extends Sprite

func _enter_tree():
	set_rotation(randf())
	set_flip_v(randi()%2)
	set_flip_h(randi()%2)


func _on_anim_animation_finished( name ):
	queue_free()
