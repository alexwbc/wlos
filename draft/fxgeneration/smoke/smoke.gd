extends Node

var particle
var particle_sub
var max_speed = 1400
var max_part = 50
var amount = 0


func subparticle():
	particle = get_node("Particles2D")
	particle_sub = particle.duplicate()
	particle_sub.set_global_position(Vector2(0,0))
#	particle_sub.set_color(Color(1,0,0,1))
	particle_sub.set_amount(40)
	particle_sub.set_emitting(false)
	particle_sub.set_param(6, 500)
	particle.add_child(particle_sub)
#	particle_sub.hide()

func _enter_tree():
	set_process_input(true)
	set_process(true)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	subparticle()

func _input(event):
	if event.type == InputEvent.MOUSE_MOTION:
		var speed = abs(event.speed_y)+abs(event.speed_x)
		var pos = particle.get_global_position()
#		atan((x2 - x1) / (y2 - y1)
#		var value= rad2deg(atan2((event.relative_pos.x-pos.x),(event.relative_pos.y-pos.y)))
		var value= atan2(event.speed_y,event.speed_x)#/PI*180
		var degr = 180-(rad2deg(value))
		var dgrr = rad2deg(value)
#		get_node("Label").set_text("atan is: " + str(value) +"\ndeg is: " +str(degr)+"\nradc is: " +str(dgrr))
		get_node("Particles2D/Sprite").set_rotation(value)
		get_node("Label").set_text(str(value))

		
#		particle.set_param(0,value)
		particle.set_param(5,degr+90)
		
		particle.set_global_position(pos+event.relative_pos)
		return

		if speed > 1000:
			particle_sub.set_emitting(true)
			particle_sub.set_param(5,degr+90)
		else:
			particle_sub.set_emitting(false)
		return
#		var old_amount = particle.get_amount()
#		amount = 10+int((min(max_speed, speed)/max_speed)*max_part)
#		print(old_amount)
#		if old_amount != amount:
#			particle.set_amount(amount)
		

func paticle_mod():
	pass

func _process(delta):
	pass
	