extends Particles2D
var particles
func _enter_tree():
	particles = get_amount()
	set_process(true)

func _process(delta):
	print(particles)
	particles -= 1*delta
	set_amount(ceil(particles))
	if particles <= 1:
		set_emitting(false)
		queue_free()