extends Node2D

var opacity = 1.0

func _enter_tree():
	set_process(true)


func _process(delta):
	opacity -= 1*delta
	set_modulate(Color(1,1,1,opacity))
	if opacity <= 0:
		queue_free()