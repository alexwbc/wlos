extends Node2D
func _enter_tree():
	set_process(true)
func _process(delta):
	var orig = get_node("ori").get_global_position()
	var dest = get_node("dest").get_global_position()
	
	get_node("dest").set_global_position(get_viewport().get_mouse_position())
	
	var direction = atan2((orig.x-dest.x), (orig.y-dest.y))
	print(direction)
	get_node("ori/Sprite").set_rotation(-direction)

