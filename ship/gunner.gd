extends Position2D

var ship
var game
var main
var db

#curve nodes
var firecurve
var follow
var bullets_speed = 100
var difference_ratio = 0.3
var max_gun_proximity = [0,15,1000]#percent: game node take care of this for us
var bg_target = null

var bullet_front


var mode = "idle"#"flat"/"curved"
#virtual nodes

#main.cursor_pos (position of the cursor)
var gunner_gpos = Vector2(0,0)#REMOVING
var target_gpos = Vector2(0,0)#REMOVING
var gunner_pos = Vector2(0,0)#only global position
var gunner_mid = Vector2(0,0)
var target_mid = Vector2(0,0)
var target_pos = Vector2(0,0)#only global position
var gunner_oldpos = Vector2(0,0)#use to fix the target motion#REMOVING
var aiming = false
var old_pos = Vector2(0,0)

var bg_fire = false
var was_bg_fire = false
#services
var motion_relative = Vector2(0,0)
#curve settings
var maxcurve_gunner = 30 #max_curve1
var maxcurve_target = 60 #max_curve2

#processing state machine
var alt_fire_mode = false

#fireline


#collision for primary gun
#var collide_target = []
var fireline#template for istacing the fireline segments
var collide_target#array contain every piece of the (five) firelines. the float value is "where" the fireline ends (the origin is taken from the previous fireline): ie: 0.5 is halfway, 1 is the end.
var fireline_obstruct = 0#"signal" the firelines give to the gunner when something is obstructing (shield or ship)

#debug nodes
var debug_sprite

func initiate_fireline():
	fireline = preload("res://ship/fireline_segment.tscn")
	collide_target = [1,[0.3,Vector2(), fireline.instance()],[0.6,Vector2(), fireline.instance()],[0.7,Vector2(), fireline.instance()],[0.8,Vector2(2,2), fireline.instance()],[1.0,Vector2(), fireline.instance()]]
	collide_target[1][2].gunner = self
	collide_target[1][2].id = 1
	collide_target[2][2].gunner = self
	collide_target[2][2].id = 2
	collide_target[3][2].gunner = self
	collide_target[3][2].id = 3
	collide_target[4][2].gunner = self
	collide_target[4][2].id = 4
	collide_target[5][2].gunner = self
	collide_target[5][2].id = 5
	#to optimize performances, the 5 firelines segment aren't placed/moved ("update" from now on) all at the same time in the same cycles
	#each cycle update move one single segment; ie: it takes up to 5 cycles to update a complete set of 5 fire segment
	#collide_target[0] tracks which segment has to be updated

	add_child(collide_target[1][2])
	add_child(collide_target[2][2])
	add_child(collide_target[3][2])
	add_child(collide_target[4][2])
	add_child(collide_target[5][2])

func _enter_tree():
	debug_sprite= preload("res://draft/sprite_vanishing.tscn")
	ship = get_parent()
	game = ship.get_parent()
	main = game.get_parent()
	ship.gunner = get_node(".")
	firecurve = get_node("firecurve")
	follow = get_node("firecurve/PathFollow2D")
	bg_target = get_node("firebg")
	bg_target.set_meta("secondary_fire",0)
	
	bullet_front = get_node("bullet")
	bullet_front.set_meta("bullet",0)
	
	target_pos = main.cursor_pos
	gunner_gpos = get_global_position()
	old_pos = gunner_gpos
	set_process(true)
	set_process_input(true)
	initiate_fireline()



func _input(ev):
	if ev.is_class("InputEventMouseMotion"):
		aiming = true
		motion_relative = ev.relative

	if ev.is_class("InputEventMouseButton"):
		if ev.is_action_pressed("alt_fire"):
			fireline_set(true)
			alt_fire_mode = true
			var limit_pos = max(main.cursor_pos.x, gunner_pos.x + max_gun_proximity[0])
			target_pos = Vector2(limit_pos, main.cursor_pos.y)
			main.hide_cursor = true
		elif ev.is_action_released("alt_fire"):
			fireline_set(false)
			
			if alt_fire_mode:
				main.cursor.set_global_position(target_pos)
				main.cursor_pos = target_pos
			alt_fire_mode = false
			main.hide_cursor = false

func shooting(delta):
	print("shooting")

func alt_fire_mode(delta):
	update_curve()
	if !aiming:
		motion_relative = Vector2(0,0)

	if !ship.idle or aiming:
		var gunner_proxim = gunner_pos.x + max_gun_proximity[0]#the gunner will be able to shoot only in front of the ship (it's origin+position of ship's head)

		target_pos += motion_relative#+(old_pos-gunner_gpos)
		target_pos = Vector2(max(min(main.screen_x_margin, target_pos.x), gunner_proxim), max(min(main.screen_y_margin, target_pos.y), 0))

####just for visualization
	get_node("targetref").set_global_position(target_pos)#
	get_node("gunref").set_global_position(gunner_pos)
####

	follow.set_unit_offset(randf())
	bullet_front.set_position(follow.get_position())
	bullet_front.set_rotation(follow.get_rotation()-1.5)

#	gunner_oldgpos = gunner_gpos

func bg_fire_mode(delta):
	bg_target.set_global_position(main.cursor_pos)
	bg_target.set_monitorable(true)


func fireline_process(delta):
	pass

func _process(delta):
	get_node("Label").set_text(str(fireline_obstruct))

#	gunner_pos = get_global_position()+Vector2(0,max_gun_proximity[0])
#	target_os = get_node("target").get_global_position()
	var bg_firing = false
	if alt_fire_mode:
		alt_fire_mode(delta)
		if fireline_obstruct > 0:
			fireline_process(delta)

	else:
#		get_node("target").set_position(Vector2(max_gun_proximity[0],0))
		if Input.is_action_pressed("main_fire"):
			bg_fire_mode(delta)
			bg_firing = true
			was_bg_fire = true
	if was_bg_fire and !bg_firing:
		bg_target.set_monitorable(false)
		was_bg_fire = false

	

	aiming = false
	old_pos = gunner_pos
	
func update_curve():#messed before I realized I could use interpolation... oh well, lerp me
	gunner_pos = get_global_position()
#	target_pos = get_node("target").get_global_position()#target should be not able to stay behind the ship (x position is limited to being keep in front of the ship)

######dummy placement
#	get_node("target").set_global_position(target_pos)
###
	var gunner_ori = Vector2(0,0)
	var difference = target_pos-gunner_pos
	gunner_mid = Vector2((difference.x*difference_ratio), difference.y)
	target_mid = Vector2((-difference.x*difference_ratio), difference.y)
	var curve = firecurve.get_curve()
	curve.clear_points()
	curve.add_point(gunner_ori, -gunner_mid, gunner_mid)
	curve.add_point(difference, target_mid, -target_mid)



	var orig = Vector2(0,0)#the segment will use the origin to determine its rotation and scale
	var dest = Vector2(0,0)#the segment will be placed here
	var step = collide_target[0]#each step is a segment, 1 being the first, 5 being the last
	if step == 5:#last one, not a real position
		orig = collide_target[step-1][1]#the origin position is take from the previous segment
		dest = target_pos#dest
		collide_target[0] = 1
	elif step == 1:#first segment
		step = 1
		orig = gunner_pos#the origin position is take from the gunner
		follow.set_unit_offset(collide_target[step][0])
		dest = follow.get_global_position()
		collide_target[step][1] = dest
		collide_target[0] = 2#next will be two
	else:
		follow.set_unit_offset(collide_target[step][0])
		orig = collide_target[step-1][1]#the origin position is take from the previous segment
		dest = follow.get_global_position()
		collide_target[step][1] = dest
		collide_target[0] += 1#next will be...
	
#	print(collide_target[step][2].get_name())
	collide_target[step][2].set_global_position(orig)
	var lenght = orig-dest
	var direction = atan2(lenght.x, lenght.y)
	var size_to_scale = 1+(abs(lenght.x)+abs(lenght.y))/50
#	if step == 1:
#		print(size_to_scale)
#		collide_target[step][2].set_scale(Vector2(1,size_to_scale))
	collide_target[step][2].set_scale(Vector2(1,size_to_scale))
	collide_target[step][2].set_rotation(-direction)
	#	print("step ora: " + str(step))
#	else:#2, 3 and 4 segment
#		collide_target[0] += 1
#	print(collide_target[0])
#	print(collide_target[0])
#	follow.set_unit_offset(collide_target[collide_target[0]][0])
#	collide_target[collide_target[0]][2].set_global_position(follow.get_global_position())
	

#	print(collide_target[collide_target[0]])
#	print(str(collide_target[collide_target[0]][0]))


	get_node("mid_one").set_position(gunner_mid)
	get_node("mid_two").set_position(difference+target_mid)


func disable():
	set_process(false)
	set_process_input(false)
	hide()

func enable():
	set_process(true)
	set_process_input(true)
	show()

func fireline_set(mode):
	if !mode:
		print("resetting")
		fireline_obstruct = 0
		for i in range(collide_target.size()):
			if i != 0:#the first one is just a counter, and not an actual fireline piece
				var segment = collide_target[i][2]
				segment.set_global_position(Vector2(-1000,-1000))
				segment.shields_on_me = []
				segment.ships_on_me = []
				
#				print(collide_target[i][2])
	pass
	
func fireline_obstruction_update(id):#a fireline segment that was obstruction the line is now free: check for the remianing firelines if there's still something obsctructiong
	var remain_size = collide_target.size()-1-id#the first one is not used. the number of other check remianing depends on fireline position (ie: "5" mean the last one... so the size (5) minus the id (5) mean we don't need to perform any check (since was the last one)
	if remain_size <= 0:
		fireline_obstruct = 0
		return
	var count = id+1
	for i in remain_size:
		var fireline_on_check = collide_target[count][2]
		if fireline_on_check.obstruction:
			fireline_obstruct = count
#			fireline_on_check.processing = true#the fireline will now work for us
			return
		count+=1
		
		