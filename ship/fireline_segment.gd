extends Area2D
#this is one of the 5 segment that will costantly drawn on the primary weapon (the curves shooting) to detect collision
var gunner
#var firing = false
var id
var shields_on_me = []
var ships_on_me = []
var obstruction = false

var processing = false
var processing_target 

func _enter_tree():
	set_process(true)

func process_against_a_shield(delta,size):
	var target = shields_on_me[0]
	if size != 1:#if there's more than one shield, we can't simply pick the first one. Checking if the other side have minor x (more closer to the left side of the screen)
		var temp_target = [target, target.get_global_position().x]
		for i in (size-1):
			var shield_check = shields_on_me[i+1]
			var xpos = shield_check.get_global_position().x
			if xpos < temp_target[1]:
				temp_target = [shield_check, xpos]
		target = temp_target[0]
	var color = target.get_modulate()
	color.v += 1*delta
	target.set_modulate(color)
	var tremble = randi()%10
	target.set_position(Vector2(tremble,tremble))
	

func process_against_a_ship(delta,size):
	var target = ships_on_me[0]
	if size != 1:#if there's more than one ship, we can't simply pick the first one. Checking if the other side have minor x (more closer to the left side of the screen)
		var temp_target = [target, target.get_global_position().x]
		for i in (size-1):
			var ship_check = ships_on_me[i+1]
			var xpos = ship_check.get_global_position().x
			if xpos < temp_target[1]:
				temp_target = [ship_check, xpos]
		target = temp_target[0]
	else:
		print(target.get_name())
####
	target = target.get_parent().get_node("Sprite")
#	print(target.get_name())
###
	var color = target.get_modulate()
	color.v += 1*delta
	target.set_modulate(color)
	var tremble = randi()%10
	target.set_position(Vector2(tremble,tremble))


func _process(delta):
#	if self == gunner.collide_target[5][2]:
#		print(ships_on_me.size())
	if gunner.fireline_obstruct == id:
		var sdom_size = shields_on_me.size()#ShielDOnMeSize
		if sdom_size > 0:
			process_against_a_shield(delta, sdom_size)
			return
		var spom_size = ships_on_me.size()#ShiPOnMeSize
		if spom_size > 0:
			process_against_a_ship(delta,spom_size)
			return



func _on_fireline_segment_area_shape_entered( area_id, area, area_shape, self_shape ):
	if !gunner.alt_fire_mode:
		return
	var obstruct_check = false
	if area.has_meta("shield"):
		obstruct_check = true
#		gunner.fireline_obstruct = true
		var current_size = shields_on_me.size()
		shields_on_me.resize(current_size+1)
		shields_on_me[current_size] = area
		
	elif area.has_meta("ship"):
		obstruct_check = true
#		gunner.fireline_obstruct = true
		var current_size = ships_on_me.size()
		ships_on_me.resize(current_size+1)
		ships_on_me[current_size] = area
	
	if obstruct_check:
		obstruction = true
		if gunner.fireline_obstruct > id or (gunner.fireline_obstruct == 0):
			gunner.fireline_obstruct = id

func _on_fireline_segment_area_shape_exited( area_id, area, area_shape, self_shape ):
	if !gunner.alt_fire_mode:
		return
	
	var free_of_obstruction = false
	if area.has_meta("shield"):#a shield exited, remove and resort the shield array
		var previous_shield_count = shields_on_me.size()
		if previous_shield_count <= 1:
			shields_on_me = []
			if ships_on_me == []:
				free_of_obstruction = true
		else:#there are more than one shield, so we have to resort the array
			var count = 0
			var shields_removed = 0
			for i in range(previous_shield_count):
				if shields_on_me[i] != area:#this is okey
					shields_on_me[count] = shields_on_me[i]
					count += 1
				else:
					shields_removed += 1
			shields_on_me.resize(previous_shield_count-shields_removed)

	if area.has_meta("ship"):#a ship exited, remove and resort the ship array
		var previous_ship_count = ships_on_me.size()
		if previous_ship_count <= 1:
			ships_on_me = []
			if shields_on_me == []:
				free_of_obstruction = true
		else:#there are more than one shield, so we have to resort the array
			var count = 0
			var ships_removed = 0
			for i in range(previous_ship_count):
				if ships_on_me[i] != area:#this is okey
					ships_on_me[count] = ships_on_me[i]
					count += 1
				else:
					ships_removed += 1
			ships_on_me.resize(previous_ship_count-ships_removed)
	
	if free_of_obstruction:
		obstruction = false
		if gunner.fireline_obstruct == id:
			gunner.fireline_obstruction_update(id)
