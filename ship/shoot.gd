extends Node2D

var path
var follow
var speed = 100
var sprite

var start
var mid_pos
var end
var mouse_pos

func _enter_tree():
	path = get_node("Path2D")
	follow = get_node("Path2D/PathFollow2D")
	sprite = get_node("Sprite")
	start = get_node("start").get_position()
	mid_pos = get_node("curve").get_position()
	end = get_node("end").get_position()
	
	set_fixed_process(true)
	set_process_input(true)
	
	#ongoing___--

func _input(ev):
	if ev.type == InputEvent.MOUSE_MOTION:
		end = get_viewport().get_mouse_position()
#		mid_pos.x = start.x
		mid_pos = start
		get_node("curve").set_position(mid_pos)
		get_node("end").set_position(end)
		update()

func _fixed_process(delta):
	start = get_node("start").get_position()
	if Input.is_action_pressed("ui_up"):
		start.y -= 100*delta
	elif Input.is_action_pressed("ui_down"):
		start.y += 100*delta
	get_node("start").set_position(start)
	if Input.is_mouse_button_pressed(1):
		print("update")
#		update(get_viewport().get_mouse_position())
#		update(delta)

	var n_offset = follow.get_offset() + delta * speed
	print(follow.get_rotation())
	follow.set_offset(n_offset)
#	follow.set_unit_offset(randf())
	sprite.set_position(follow.get_position())
	sprite.set_rotation(follow.get_rotation()-1.5)

func update():
	var curve = path.get_curve()
	curve.clear_points()
#	curve.add_point(get_node("start").get_position(), mousepos)
	curve.add_point(start)
	curve.add_point(end,Vector2(0, -(end.y-mid_pos.y)))

