extends Node2D

var root
var main
var db
var game
var gunner = null#gunner module (if any) will place itself here

#firepower
var fp_primary = 20
#var fp_secondary = [8, 0.08]#power/firerate
#var fp_secondary = [8, 0.08]
var fp_secondary = [10, 0.03]

#health process
var health = 100
var health_recover = 30

var health_delay = [0,1]

var limit_x = Vector2(10, 50)
var limit_y = Vector2(10, 90)

var set_limit_x = Vector2(0,0)#game set this for us
var set_limit_y = Vector2(0,0)#game set this for us
var field_size = Vector2(0,0)#game set this for us
var motion = Vector2(10,1)

var ship_speed = 250
var ship_decel = 0.9

#anim
var burst = "front"
var updown = "up"
var old_anim
var anim = "idle"
var was_flat = false

#status processing
var idle = false
var novel = false#cinematic scene
var idle_novel = false

var cinematic_path = null
var cinematic_path_follow = null

func _enter_tree():
	set_process(true)
	main = get_node("/root/main")
	db = main.database
	main.player = get_node(".")
	inflict_damage(10)

func damage_process(delta):
	var healthbar = get_node("healthbar")

	if health_delay[0] <= 0:
		if health < 98:
			health += health_recover*delta
			get_node("healthbar/label").set_text("health: " +str(health) +"delay : " + str(health_delay))
		elif health < 99:
			var opacity = healthbar.get_modulate()
			get_node("healthbar/label").set_text("health: 100")
			opacity.a -= 1*delta
			if opacity.a <= 0:
				healthbar.hide()
				health = 100
			else:
				get_node("healthbar/label").set_text("health: 100")
				healthbar.set_modulate(opacity)
	else:
		get_node("healthbar/label").set_text("health: " +str(health) +"delay : " + str(health_delay[0]))
		health_delay[0] -= 1*delta
	healthbar.set("value", health)
func game_over():
	pass

func inflict_damage(amount):
	if novel:
		return
	if (health-amount) <= 0:
		game_over()
		return
	get_node("healthbar").set_modulate(Color(1,1,1,1))
	get_node("healthbar").show()
	health -= amount
	health_delay[0] = health_delay[1]



func process_novel(delta):
	var offset = cinematic_path_follow.get_unit_offset()
	if idle_novel:
		cinematic_path_follow.set_unit_offset(0)
	else:
		if offset <= 0.9:
			offset += 1*delta
		else:
			idle_novel = true
			offset = 0.9
		cinematic_path_follow.set_unit_offset(offset)
		var shipnovel_pos = cinematic_path_follow.get_position()
		set_position(shipnovel_pos)
		game.ship_percent_x = (shipnovel_pos.x - set_limit_x[0])/field_size.x
		game.ship_percent_y = (shipnovel_pos.y - set_limit_y[0])/field_size.y




func cinematic_mode(turn_on, waitloc):#the waitloc is in percent
	if turn_on:#activate the "visual novel" mode
		if novel and !idle_novel:#there's a previous motion. warp to end of it before begin a new one
			cinematic_path_follow.set_unit_offset(1)
			var shipnovel_pos = cinematic_path_follow.get_position()
			set_position(shipnovel_pos)
			game.ship_percent_x = (shipnovel_pos.x - set_limit_x[0])/field_size.x
			game.ship_percent_y = (shipnovel_pos.y - set_limit_y[0])/field_size.y

		novel = true
		idle_novel = false
			
		#create path with two point: where the ship is (start_path) and where the ship will "wait" during the novel scene
		var start_path = get_global_position()
		var target_path = Vector2(waitloc.x*main.screen_x_ratio, waitloc.y*main.screen_y_ratio)
		cinematic_path = Path2D.new()
		cinematic_path.get_curve().add_point(start_path)
		cinematic_path.get_curve().add_point(target_path)
		add_child(cinematic_path)
		cinematic_path.set_owner(get_node("."))
		cinematic_path_follow = PathFollow2D.new()
		cinematic_path.add_child(cinematic_path_follow)
		cinematic_path_follow.set_owner(cinematic_path)
		if gunner != null:
			gunner.disable()

	else:#disable the visual novel
		novel = false
		cinematic_path_follow.queue_free()
		cinematic_path_follow = null
		cinematic_path.queue_free()
		cinematic_path = null
		motion = Vector2(0,0)
		if gunner != null:
			gunner.enable()

func _process(delta):
	if novel:
		if was_flat:
			was_flat = false
			anim = "return." + str(updown) + str(burst)
			get_node("ship_anim").play(anim)
			old_anim = anim
		process_novel(delta)
		return
	var ship_pos = get_position()

	var flat = true
	
	if health < 99:
		damage_process(delta)
	
	if Input.is_action_pressed("key_descent"):
		updown = "dw"
		flat = false
		motion.y = ship_speed*delta
		
	elif Input.is_action_pressed("key_raise"):
		updown = "up"
		flat = false
		motion.y = -ship_speed*delta

	
	if Input.is_action_pressed("key_forward"):
		motion.x = ship_speed*delta
		burst = "front"
	elif Input.is_action_pressed("key_backward"):
		motion.x = -ship_speed*delta
		burst = "back"


	if abs(motion.x) > 0.1:
		motion.x *= ship_decel
		game.ship_percent_x = (ship_pos.x - set_limit_x[0])/field_size.x
	else:
		motion.x = 0
	if abs(motion.y) > 0.1:
		game.ship_percent_y = (ship_pos.y - set_limit_y[0])/field_size.y
		motion.y *= ship_decel
	else:
		motion.y = 0
	
	if motion == Vector2(0,0):
		idle = true
	else:
		idle = false

	ship_pos += motion

	#correct borders
#pitch = max(min(pitch - ie.relative_y * view_sensitivity, 85), -85)
	#max(min(in_max, in_val), in_min)
	ship_pos = Vector2(max(min(set_limit_x[1], ship_pos.x), set_limit_x[0]), max(min(set_limit_y[1], ship_pos.y), set_limit_y[0]))
	set_position(ship_pos)
	
#	print("x percent is: " + str(field_size.x) +" - "+  str(ship_pos.x - set_limit_x[0]))
#	print("x percent is: " + str((ship_pos.x - set_limit_x[0])/field_size.x))
	
	if flat and was_flat:#noup/down once
		was_flat = false
		anim = "return." + str(updown) + str(burst)
	if !flat:
		was_flat = true
		anim = "going." + str(updown) + str(burst)
	if !flat and !was_flat:
		anim = "switch." + str(updown) + str(burst)

	if old_anim != anim:
		get_node("ship_anim").play(anim)
		if !get_node("ship_anim").has_animation(anim):
			print("missing anim: " +str(anim))

	old_anim = anim
